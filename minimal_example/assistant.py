#!/usr/bin/env python3

from vosk import Model, KaldiRecognizer
import pyaudio
import pyttsx3
from time import sleep
import datetime as dt
import json


# text to speech
keyword = 'assistant'
assistant = pyttsx3.init(driverName='espeak')

# speech to text
path_to_speech_model = 'model_en'   # download a model from https://alphacephei.com/vosk/models
model = Model(path_to_speech_model)
kaldiRecognizer = KaldiRecognizer(model, 16000)

# start listening to microphone
portAudio = pyaudio.PyAudio()
stream = portAudio.open(format=pyaudio.paInt16, channels=1, rate=16000, input=True, frames_per_buffer=8000)
stream.start_stream()


def recognize_voice(recognizer):
    while True:
        if stream.is_active():
            data = stream.read(4000)
            if recognizer.AcceptWaveform(data):
                result = json.loads(recognizer.FinalResult())['text']
                print(result)
                return result


def speak(text, pause=0.5):
    stream.stop_stream()
    assistant.say(text)
    assistant.runAndWait()
    sleep(pause)
    stream.start_stream()


def tell_time(result):
    if 'how late' in result:
        now = dt.datetime.now()
        hour = now.strftime('%H')
        minute = now.strftime('%M')
        speak('It is ' + hour + ' ' + minute)


def start_assistant(rate=165, voice_id='english'):
    assistant.setProperty('rate', rate)
    assistant.setProperty('voice', voice_id)

    print(keyword + ' has booted..')
    speak('Hello, my name is ' + keyword)

    print('rate', assistant.getProperty('rate'))
    print('voice', assistant.getProperty('voice'))
    while True:
        result = recognize_voice(kaldiRecognizer)
        if keyword in result:
            tell_time(result)


start_assistant()
