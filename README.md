# Seminar Trip Hackathon

Did you ever find yourself yelling at your computer, but nothing happened? Well, now you have the opportunity to change that. This hackathon is intended to let you create your personal voice assistant.

## General requirements

- Laptop
- Microphone or better: Headset
- Python
- an IDE of your choice
- optional: Camera (external or built-in)
- optional: Pinboard, connecting cables, LEDs (only Raspberry)

## Tutorials
- https://itnext.io/how-to-build-your-own-python-voice-assistant-thecodingpie-eaa1f70aabb6
- https://www.geeksforgeeks.org/voice-assistant-using-python/
- https://pythongeeks.org/python-voice-assistant-project/

## Example
If you don't know where to start, check out the minimal example. It shows how to create an english speaking voice assistant using pyaudio, vosk and pyttsx3. Install the dependencies (preferably in a virtual environment), run the code and ask 'Assistent, how late is it?'.

## Libraries

### Recognition/Speech2Text

- [Vosk](https://alphacephei.com/vosk/) (offline, lightweight, multiple languages)
- [SpeechRecognition](https://github.com/Uberi/speech_recognition#readme) (used in most tutorials)
- [pyaudio](https://people.csail.mit.edu/hubert/pyaudio/)
- [assemblyai](https://github.com/assemblyai/assemblyai-python-sdk)
- [google-cloud-speech](https://github.com/googleapis/python-speech)
- [watson-developer-cloud](https://github.com/watson-developer-cloud/python-sdk)
- [wit](https://github.com/wit-ai/pywit)

### Text2Speech
- [pyttsx3](https://github.com/nateshmbhat/pyttsx3)
- [google text to speech (not affiliated with google)](https://github.com/pndurette/gTTS)

## General tips

- choose your keywords carefully
- recommended speech recognition system: either vosk or sr

## Inspiration

Some ideas what you could let your voice assistant do:
- control computer, e.g. [shutdown](https://stackoverflow.com/a/34050751), sleep ([subprocess](https://docs.python.org/3/library/subprocess.html))
- [set alarm](https://holypython.com/5-ways-to-create-an-alarm-in-python/)
- [translate stuff](https://medium.com/analytics-vidhya/how-to-translate-text-with-python-9d203139dcf5)
- [get time/date (datetime)](https://docs.python.org/3/library/datetime.html)
- search web ([wolframalpha](https://github.com/jaraco/wolframalpha), [wikipedia](https://github.com/goldsmith/Wikipedia), webbrowser, [beautifulsoup](https://www.crummy.com/software/BeautifulSoup/))
- [weather forecast](https://github.com/null8626/python-weather)
- read news/sport results
- play a song ([pywhatkit](https://github.com/Ankit404butfound/PyWhatKit))
- check stock market
- call/message someone ([twilio](https://github.com/twilio/twilio-python/), [pywhatkit](https://github.com/Ankit404butfound/PyWhatKit))
- use camera ([ecapture](https://github.com/YFOMNN/ecapture))
- recognize a song (hard)
- turn on an LED on Raspberry ([gpiozero](https://github.com/gpiozero/gpiozero))
- change voice settings, change language
- process camera input with ML model
